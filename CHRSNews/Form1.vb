﻿
Imports Microsoft.Office.Interop.Word
Imports System.Windows.Forms

Imports Newtonsoft.Json




Public Class MainForm



    Public PDM As New PersistantDataManager()

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub subSaveOnExit() Handles MyBase.Closing
        PDM.subWriteToArchiveFile()
    End Sub

    Public Sub subAddNewPiece(ByVal piece As NewsPiece)

        PDM.doc.subAddPiece(piece)

        PDM.doc.subCreateHeader()

        rtbOutput.Rtf = PDM.doc.fncReturnRTF()

        'Dim txt As String = ""
        'For Each item As NewsPiece In Me.doc.nspNewsPieces
        'txt = txt + ", " + item.fncReturnTitle()
        'Next
        'rtbOutput.Text = txt

        subUpdateNewspieceList()

        PDM.subWriteToArchiveFile()

    End Sub


    Private Sub ClearOutputToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearOutputToolStripMenuItem.Click
        rtbOutput.Text = ""




    End Sub


    Private Sub ExportToMSWordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportToMSWordToolStripMenuItem.Click
        Dim Path As String = My.Application.Info.DirectoryPath + "\Newsletter.rtf"

        Dim sw As New System.IO.StreamWriter(Path)

        sw.Write(rtbOutput.Rtf)
        sw.Flush()

        sw.Close()



        Dim WordApp As Microsoft.Office.Interop.Word.Application
        Dim WordDoc As Microsoft.Office.Interop.Word.Document

        WordApp = CreateObject("Word.Application")
        WordDoc = WordApp.Documents.Open(Path)
        WordApp.Visible = True

    End Sub

    Public Sub subUpdateNewspieceList()
        lstNewspieces.Items.Clear()





        For Each item As NewsPiece In PDM.doc.nspNewsPieces
            lstNewspieces.Items.Add(item.fncReturnTitle)
        Next
    End Sub

    Private Sub NewspieceToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewspieceToolStripMenuItem.Click
        Dim form As New NewPieceForm()



        form.frmPresentor = Me
        form.ShowDialog()
        'Me.Hide()
    End Sub

    Public Sub subLoadPersistantData()
        If PDM.fncTagsFileIsAvailable() Then
            PDM.subLoadTagsFromFile()
        End If

        If PDM.fncArchiveFileIsAvailable() Then
            PDM.subLoadArchiveFromFile()
        End If
    End Sub

    Public Sub subReloadData()
        rtbOutput.Rtf = ""



        rtbOutput.Rtf = PDM.doc.fncReturnRTF()

        subUpdateNewspieceList()
    End Sub


    Private Sub subHideNewspieceInfo()
        lblStaticTitle.Hide()
        lblTitle.Hide()

        lblStaticSubtitle.Hide()
        lblSubtitle.Hide()

        lblStaticDescription.Hide()
        rtbDescription.Hide()

        lblStaticDate.Hide()
        lblDate.Hide()

        lblStaticTime.Hide()
        lblTime.Hide()

        lblStaticTags.Hide()
        rtbTags.Hide()

        cmdDelete.Hide()
        cmdEdit.Hide()
        cmdMoveUp.Hide()
        cmdMoveDown.Hide()
    End Sub


    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, MyBase.Shown
        Me.CenterToScreen()


        subLoadPersistantData()
        subReloadData()
        subHideNewspieceInfo()

    End Sub

    Private Sub TagsToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TagsToolStripMenuItem1.Click
        Dim form As New TagCreation()

        form.frmMain = Me

        form.ShowDialog()
    End Sub

    Private Sub SetupToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetupToolStripMenuItem.Click
        Dim form As New SetupWindowsForm()

        form.PDM = Me.PDM
        form.frmMain = Me

        form.ShowDialog()
    End Sub

    Private Sub subShowNewspieceInfo(ByVal piece As NewsPiece)
        lblStaticTitle.Show()
        lblStaticSubtitle.Show()
        lblStaticDescription.Show()
        lblStaticDate.Show()
        lblStaticTime.Show()
        lblStaticTags.Show()

        cmdDelete.Show()
        cmdEdit.Show()
        cmdMoveUp.Show()
        cmdMoveDown.Show()

        lblTitle.Show()
        lblSubtitle.Show()
        rtbDescription.Show()
        lblDate.Show()
        lblTime.Show()
        rtbTags.Show()

        lblTitle.Text = piece.fncReturnTitle()
        If piece.fncReturnSubtitle() = "" Then
            lblSubtitle.Text = "N/A"
        Else
            lblSubtitle.Text = piece.fncReturnSubtitle()
        End If
        rtbDescription.Text = piece.fncReturnDescription()

        If piece.fncReturnFormattedDate() = "" Then
            lblDate.Text = "N/A"
        Else
            lblDate.Text = piece.fncReturnFormattedDate()
        End If

        If piece.fncReturnFormattedTime() = "" Then

            lblTime.Text = "N/A"

        Else

            lblTime.Text = piece.fncReturnFormattedTime()
        End If

        If piece.tghTags.fncReturnTagsAsOneString() = "" Then
            rtbTags.Text = "N/A"

        Else
            rtbTags.Text = piece.tghTags.fncReturnTagsAsOneString()

        End If


    End Sub


    Private Sub lstNewspieces_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstNewspieces.SelectedIndexChanged
        If lstNewspieces.SelectedIndex > -1 Then
            subShowNewspieceInfo(PDM.doc.nspNewsPieces(lstNewspieces.SelectedIndex))
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lstNewspieces.SelectedIndex > -1 Then
            PDM.doc.subRemoveNewspieceAt(lstNewspieces.SelectedIndex)
            Dim currentIndex As Integer = lstNewspieces.SelectedIndex
            subReloadData()
            If currentIndex - 1 > -1 Then
                lstNewspieces.SetSelected(currentIndex - 1, True)
            End If

        End If
    End Sub

    Private Sub cmdMoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMoveUp.Click

        If lstNewspieces.SelectedIndex - 1 > -1 Then
            Dim currentIndex As Integer = lstNewspieces.SelectedIndex
            PDM.doc.subMoveNewspieceUpAt(currentIndex)
            subReloadData()
            lstNewspieces.SetSelected(currentIndex - 1, True)


        End If

    End Sub

    Private Sub cmdMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMoveDown.Click

        If lstNewspieces.SelectedIndex + 1 < lstNewspieces.Items.Count Then
            Dim currentIndex As Integer = lstNewspieces.SelectedIndex
            PDM.doc.subMoveNewspieceDownAt(currentIndex)
            subReloadData()

            lstNewspieces.SetSelected(currentIndex + 1, True)

        End If

    End Sub

    Public Sub subSaveChangesToPieceAt(ByVal index As Integer, ByVal piece As NewsPiece)

        PDM.doc.nspNewsPieces.RemoveAt(index)
        PDM.doc.nspNewsPieces.Insert(index, piece)
        Dim currentIndex As Integer = lstNewspieces.SelectedIndex
        subReloadData()
        lstNewspieces.SetSelected(currentIndex, True)
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If lstNewspieces.SelectedIndex > -1 And lstNewspieces.SelectedIndex < PDM.doc.nspNewsPieces.Count Then
            Dim form As New NewPieceForm()



            form.frmPresentor = Me
            form.strRole = "edit"

            form.piece = PDM.doc.nspNewsPieces(lstNewspieces.SelectedIndex)
            form.intIndex = lstNewspieces.SelectedIndex

            form.ShowDialog()
            'Me.Hide()
        End If
    End Sub

    
    
    Private Sub Panel2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel2.Paint

    End Sub

    Private Sub MenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub rtbTags_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtbTags.TextChanged

    End Sub
End Class
