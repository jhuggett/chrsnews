﻿Public Class TagCreation

    Public frmMain As MainForm

    Public frmNewsPiece As NewPieceForm

    Public boolPresentedByNewPiece As Boolean = False

    Public PDM As PersistantDataManager

    Private tghTags As New TagHandler()

    Private Sub TagCreation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToParent()
        PDM = frmMain.PDM

        For Each item As String In PDM.fncReturnTags().fncReturnTags()
            tghTags.subAddTag(item)
        Next

        subDisplayAllTags()

        cmdEdit.Hide()
        cmdRemove.Hide()
        lblTagName.Hide()
        lblStaticTagName.Hide()

    End Sub

    Private Sub SaveChanges() Handles MyBase.Closing

        PDM.subWriteToTagsFile(Me.tghTags)

        subUpdateNewPiece()
    End Sub

    Private Sub subDisplayAllTags()
        lstTags.Items.Clear()
        For Each item As String In tghTags.fncReturnTags()
            lstTags.Items.Add(item)
        Next


    End Sub

    Private Sub subUpdateNewPiece()
        If Me.boolPresentedByNewPiece Then
            frmNewsPiece.subDisplayTags()
        End If
    End Sub

    Private Sub cmdAddNewTag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddNewTag.Click

        cmdEdit.Hide()
        cmdRemove.Hide()
        lblTagName.Hide()
        lblStaticTagName.Hide()

        Dim strNewTag As String = InputBox("Enter the Tag (CANNOT contain commas):", "Enter Tag")

        If Not strNewTag.Contains(",") And Not strNewTag = "" Then
            tghTags.subAddTag(strNewTag)
        End If

        subDisplayAllTags()
    End Sub

    Private Sub lstTags_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstTags.SelectedIndexChanged
        

        If lstTags.SelectedIndex < tghTags.fncReturnTags().Count And lstTags.SelectedIndex > -1 Then
            lblTagName.Text = tghTags.fncReturnTags()(lstTags.SelectedIndex)
            lblTagName.Show()
            cmdRemove.Show()
            cmdEdit.Show()
            lblStaticTagName.Show()
        End If

        'lblTagName.Text = lstTags.SelectedIndex.ToString

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim strEditedTag As String = InputBox("Edit the Tag (CANNOT contain commas):", "Edit Tag", Me.tghTags.fncReturnTags()(lstTags.SelectedIndex))
        Dim index As Integer = tghTags.fncReturnTags().IndexOf(tghTags.fncReturnTags()(lstTags.SelectedIndex))
        If Not strEditedTag.Contains(",") And Not strEditedTag = "" Then
            tghTags.subSwitchTags(index, strEditedTag)
        End If


        subDisplayAllTags()
        lstTags.SetSelected(index, True)
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Me.tghTags.subRemove(tghTags.fncReturnTags()(lstTags.SelectedIndex))

        cmdEdit.Hide()
        cmdRemove.Hide()
        lblTagName.Hide()
        lblStaticTagName.Hide()

        subDisplayAllTags()
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        SaveChanges()
        Me.Close()
    End Sub

    Private Sub lblTagName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblTagName.Click

    End Sub

    Private Sub lblStaticTagName_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblStaticTagName.Click

    End Sub

    Private Sub lblCurrentTags_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblCurrentTags.Click

    End Sub
End Class