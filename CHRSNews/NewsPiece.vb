﻿
Public Class NewsPiece
    Public strTitle As String
    Public strSubtitle As String
    Public strDescription As String

    Public dteDate As Date = Date.MinValue
    Public dteTime As Date = Date.MinValue




    Public tghTags As New TagHandler()

    Public Sub subInit(ByVal strTitle As String, ByVal strDescription As String, ByVal strSubTitle As String, ByVal strTags As String)

        Me.strTitle = strTitle
        Me.strSubtitle = strSubTitle
        Me.strDescription = strDescription

        Dim tags As String() = strTags.Split(New Char() {","c})
        For Each tag In tags
            Me.tghTags.subAddTag(tag)
        Next

    End Sub

    Public Sub subSetDate(ByVal dateToSet As Date)
        Me.dteDate = dateToSet
    End Sub

    Public Sub subSetTime(ByVal timeToSet As Date)
        Me.dteTime = timeToSet
    End Sub

    Public Function fncReturnTitle() As String
        Return Me.strTitle
    End Function

    Public Function fncReturnSubtitle() As String
        Return Me.strSubtitle
    End Function

    Public Function fncReturnDescription() As String
        Return Me.strDescription
    End Function

    Public Function fncReturnFormattedDate() As String
        Dim strDate As String = ""
        If dteDate = Date.MinValue Then
            Return strDate
        End If
        Dim currentDate As DateTime = Me.dteDate
        Dim daysOfWeek() As String = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}
        strDate += daysOfWeek(currentDate.DayOfWeek)
        strDate += ", "

        Dim monthsOfYear() As String = {"January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
        strDate += monthsOfYear(currentDate.Month - 1)
        strDate += " "

        strDate += CInt(currentDate.Day).ToString
        If currentDate.Day = 1 Then
            strDate += "st"
        ElseIf currentDate.Day = 2 Then
            strDate += "nd"
        ElseIf currentDate.Day = 3 Then
            strDate += "rd"
        Else
            strDate += "th"
        End If
        'strDate += ", "
        'strDate += CInt(currentDate.Year).ToString

        Return strDate
    End Function

    Public Function fncReturnFormattedTime() As String
        Dim strTime As String = ""
        If dteTime = Date.MinValue Then
            Return strTime
        End If

        Dim strHours As String = ""
        Dim strPeriod As String = "AM"

        Dim intHours As Integer = dteTime.Hour


        If intHours > 12 Then
            intHours -= 12
            strPeriod = "PM"
        End If

        strHours += intHours.ToString()

        Dim strMinutes As String = ""
        If dteTime.Minute < 10 Then
            strMinutes += "0"
        End If


        strMinutes += dteTime.Minute.ToString()
        strTime += strHours + ":" + strMinutes + " " + strPeriod

       


        Return strTime
    End Function



End Class

Public Class TagHandler

    Public strTags As New ArrayList()

    Public Sub subAddTag(ByVal tag As String)
        strTags.Add(tag)
    End Sub

    Public Function fncReturnTags() As ArrayList
        Return Me.strTags
    End Function

    Public Sub subEmpty()
        Me.strTags.Clear()
    End Sub

    Public Sub subRemove(ByVal tag As String)
        If Me.strTags.Contains(tag) Then
            Me.strTags.Remove(tag)
        End If
    End Sub

    Public Sub subSwitchTags(ByVal at As Integer, ByVal tag As String)
        Me.strTags.RemoveAt(at)
        Me.strTags.Insert(at, tag)
    End Sub

    Public Function fncReturnTagsAsOneString() As String
        Dim strAllTags As String = ""

        If Me.strTags.Count = 0 Then
            Return ""
        End If

        If Me.strTags.Count = 1 Then
            Return Me.strTags.ToArray().First()
        End If

        For i = 0 To Me.strTags.Count - 2
            strAllTags += Me.strTags.Item(i) + ", "
        Next
        strAllTags += Me.strTags.ToArray().Last()

        Return strAllTags
    End Function

    Public Function fncDoesContain(ByVal tag As String) As Boolean

        For Each item As String In strTags
            If item = tag Then
                Return True
            End If
        Next

        Return False
    End Function

End Class
