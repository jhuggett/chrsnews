﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rtbOutput = New System.Windows.Forms.RichTextBox
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.NewspieceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ClearOutputToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TagsToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ExportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExportToMSWordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.lstNewspieces = New System.Windows.Forms.ListBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lblStaticTags = New System.Windows.Forms.Label
        Me.cmdMoveUp = New System.Windows.Forms.Button
        Me.rtbTags = New System.Windows.Forms.RichTextBox
        Me.cmdMoveDown = New System.Windows.Forms.Button
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblStaticDescription = New System.Windows.Forms.Label
        Me.lblSubtitle = New System.Windows.Forms.Label
        Me.lblStaticTitle = New System.Windows.Forms.Label
        Me.rtbDescription = New System.Windows.Forms.RichTextBox
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.lblStaticSubtitle = New System.Windows.Forms.Label
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblStaticDate = New System.Windows.Forms.Label
        Me.lblTime = New System.Windows.Forms.Label
        Me.lblStaticTime = New System.Windows.Forms.Label
        Me.MenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'rtbOutput
        '
        Me.rtbOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbOutput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.rtbOutput.Location = New System.Drawing.Point(353, 24)
        Me.rtbOutput.Name = "rtbOutput"
        Me.rtbOutput.ReadOnly = True
        Me.rtbOutput.Size = New System.Drawing.Size(666, 636)
        Me.rtbOutput.TabIndex = 0
        Me.rtbOutput.Text = ""
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.EditToolStripMenuItem, Me.ExportToolStripMenuItem, Me.HelpToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1023, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewspieceToolStripMenuItem})
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(43, 20)
        Me.NewToolStripMenuItem.Text = "New"
        '
        'NewspieceToolStripMenuItem
        '
        Me.NewspieceToolStripMenuItem.Name = "NewspieceToolStripMenuItem"
        Me.NewspieceToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.NewspieceToolStripMenuItem.Text = "Newspiece"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearOutputToolStripMenuItem, Me.TagsToolStripMenuItem1})
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'ClearOutputToolStripMenuItem
        '
        Me.ClearOutputToolStripMenuItem.Name = "ClearOutputToolStripMenuItem"
        Me.ClearOutputToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.ClearOutputToolStripMenuItem.Text = "Clear Output"
        '
        'TagsToolStripMenuItem1
        '
        Me.TagsToolStripMenuItem1.Name = "TagsToolStripMenuItem1"
        Me.TagsToolStripMenuItem1.Size = New System.Drawing.Size(142, 22)
        Me.TagsToolStripMenuItem1.Text = "Tags"
        '
        'ExportToolStripMenuItem
        '
        Me.ExportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportToMSWordToolStripMenuItem})
        Me.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem"
        Me.ExportToolStripMenuItem.Size = New System.Drawing.Size(52, 20)
        Me.ExportToolStripMenuItem.Text = "Export"
        '
        'ExportToMSWordToolStripMenuItem
        '
        Me.ExportToMSWordToolStripMenuItem.Name = "ExportToMSWordToolStripMenuItem"
        Me.ExportToMSWordToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.ExportToMSWordToolStripMenuItem.Text = "Export to MSWord"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem, Me.SetupToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'SetupToolStripMenuItem
        '
        Me.SetupToolStripMenuItem.Name = "SetupToolStripMenuItem"
        Me.SetupToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.SetupToolStripMenuItem.Text = "Setup"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 663)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1023, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(66, 17)
        Me.ToolStripStatusLabel1.Text = "Version: 0.3"
        '
        'lstNewspieces
        '
        Me.lstNewspieces.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lstNewspieces.FormattingEnabled = True
        Me.lstNewspieces.Location = New System.Drawing.Point(12, 24)
        Me.lstNewspieces.Name = "lstNewspieces"
        Me.lstNewspieces.Size = New System.Drawing.Size(336, 329)
        Me.lstNewspieces.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel2.Controls.Add(Me.lblStaticTags)
        Me.Panel2.Controls.Add(Me.cmdMoveUp)
        Me.Panel2.Controls.Add(Me.rtbTags)
        Me.Panel2.Controls.Add(Me.cmdMoveDown)
        Me.Panel2.Controls.Add(Me.lblTitle)
        Me.Panel2.Controls.Add(Me.lblStaticDescription)
        Me.Panel2.Controls.Add(Me.lblSubtitle)
        Me.Panel2.Controls.Add(Me.lblStaticTitle)
        Me.Panel2.Controls.Add(Me.rtbDescription)
        Me.Panel2.Controls.Add(Me.cmdEdit)
        Me.Panel2.Controls.Add(Me.lblStaticSubtitle)
        Me.Panel2.Controls.Add(Me.cmdDelete)
        Me.Panel2.Controls.Add(Me.lblDate)
        Me.Panel2.Controls.Add(Me.lblStaticDate)
        Me.Panel2.Controls.Add(Me.lblTime)
        Me.Panel2.Controls.Add(Me.lblStaticTime)
        Me.Panel2.Location = New System.Drawing.Point(12, 363)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(336, 297)
        Me.Panel2.TabIndex = 15
        '
        'lblStaticTags
        '
        Me.lblStaticTags.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblStaticTags.AutoSize = True
        Me.lblStaticTags.Location = New System.Drawing.Point(17, 198)
        Me.lblStaticTags.Name = "lblStaticTags"
        Me.lblStaticTags.Size = New System.Drawing.Size(34, 13)
        Me.lblStaticTags.TabIndex = 8
        Me.lblStaticTags.Text = "Tags:"
        '
        'cmdMoveUp
        '
        Me.cmdMoveUp.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdMoveUp.Location = New System.Drawing.Point(258, 10)
        Me.cmdMoveUp.Name = "cmdMoveUp"
        Me.cmdMoveUp.Size = New System.Drawing.Size(75, 23)
        Me.cmdMoveUp.TabIndex = 12
        Me.cmdMoveUp.Text = "Move Up"
        Me.cmdMoveUp.UseVisualStyleBackColor = True
        '
        'rtbTags
        '
        Me.rtbTags.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rtbTags.Location = New System.Drawing.Point(5, 214)
        Me.rtbTags.Name = "rtbTags"
        Me.rtbTags.ReadOnly = True
        Me.rtbTags.Size = New System.Drawing.Size(328, 35)
        Me.rtbTags.TabIndex = 13
        Me.rtbTags.Text = ""
        '
        'cmdMoveDown
        '
        Me.cmdMoveDown.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdMoveDown.Location = New System.Drawing.Point(258, 39)
        Me.cmdMoveDown.Name = "cmdMoveDown"
        Me.cmdMoveDown.Size = New System.Drawing.Size(75, 23)
        Me.cmdMoveDown.TabIndex = 12
        Me.cmdMoveDown.Text = "Move Down"
        Me.cmdMoveDown.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Location = New System.Drawing.Point(53, 20)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(69, 13)
        Me.lblTitle.TabIndex = 4
        Me.lblTitle.Text = "Not Provided"
        '
        'lblStaticDescription
        '
        Me.lblStaticDescription.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblStaticDescription.AutoSize = True
        Me.lblStaticDescription.Location = New System.Drawing.Point(17, 67)
        Me.lblStaticDescription.Name = "lblStaticDescription"
        Me.lblStaticDescription.Size = New System.Drawing.Size(63, 13)
        Me.lblStaticDescription.TabIndex = 6
        Me.lblStaticDescription.Text = "Description:"
        '
        'lblSubtitle
        '
        Me.lblSubtitle.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblSubtitle.AutoSize = True
        Me.lblSubtitle.Location = New System.Drawing.Point(53, 45)
        Me.lblSubtitle.Name = "lblSubtitle"
        Me.lblSubtitle.Size = New System.Drawing.Size(69, 13)
        Me.lblSubtitle.TabIndex = 4
        Me.lblSubtitle.Text = "Not Provided"
        '
        'lblStaticTitle
        '
        Me.lblStaticTitle.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblStaticTitle.AutoSize = True
        Me.lblStaticTitle.Location = New System.Drawing.Point(17, 20)
        Me.lblStaticTitle.Name = "lblStaticTitle"
        Me.lblStaticTitle.Size = New System.Drawing.Size(30, 13)
        Me.lblStaticTitle.TabIndex = 4
        Me.lblStaticTitle.Text = "Title:"
        '
        'rtbDescription
        '
        Me.rtbDescription.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rtbDescription.Location = New System.Drawing.Point(5, 83)
        Me.rtbDescription.Name = "rtbDescription"
        Me.rtbDescription.ReadOnly = True
        Me.rtbDescription.Size = New System.Drawing.Size(328, 66)
        Me.rtbDescription.TabIndex = 9
        Me.rtbDescription.Text = ""
        '
        'cmdEdit
        '
        Me.cmdEdit.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdEdit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdEdit.Location = New System.Drawing.Point(260, 271)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 10
        Me.cmdEdit.Text = "Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'lblStaticSubtitle
        '
        Me.lblStaticSubtitle.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblStaticSubtitle.AutoSize = True
        Me.lblStaticSubtitle.Location = New System.Drawing.Point(2, 45)
        Me.lblStaticSubtitle.Name = "lblStaticSubtitle"
        Me.lblStaticSubtitle.Size = New System.Drawing.Size(45, 13)
        Me.lblStaticSubtitle.TabIndex = 5
        Me.lblStaticSubtitle.Text = "Subtitle:"
        '
        'cmdDelete
        '
        Me.cmdDelete.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdDelete.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.cmdDelete.Location = New System.Drawing.Point(179, 271)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 11
        Me.cmdDelete.Text = "Delete"
        Me.cmdDelete.UseVisualStyleBackColor = True
        '
        'lblDate
        '
        Me.lblDate.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblDate.AutoSize = True
        Me.lblDate.Location = New System.Drawing.Point(53, 161)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(69, 13)
        Me.lblDate.TabIndex = 4
        Me.lblDate.Text = "Not Provided"
        '
        'lblStaticDate
        '
        Me.lblStaticDate.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblStaticDate.AutoSize = True
        Me.lblStaticDate.Location = New System.Drawing.Point(17, 161)
        Me.lblStaticDate.Name = "lblStaticDate"
        Me.lblStaticDate.Size = New System.Drawing.Size(33, 13)
        Me.lblStaticDate.TabIndex = 7
        Me.lblStaticDate.Text = "Date:"
        '
        'lblTime
        '
        Me.lblTime.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(57, 174)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(69, 13)
        Me.lblTime.TabIndex = 4
        Me.lblTime.Text = "Not Provided"
        '
        'lblStaticTime
        '
        Me.lblStaticTime.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblStaticTime.AutoSize = True
        Me.lblStaticTime.Location = New System.Drawing.Point(17, 174)
        Me.lblStaticTime.Name = "lblStaticTime"
        Me.lblStaticTime.Size = New System.Drawing.Size(33, 13)
        Me.lblStaticTime.TabIndex = 8
        Me.lblStaticTime.Text = "Time:"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1023, 685)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lstNewspieces)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.rtbOutput)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MinimumSize = New System.Drawing.Size(674, 473)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CRHS News"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rtbOutput As System.Windows.Forms.RichTextBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClearOutputToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ExportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportToMSWordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewspieceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TagsToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SetupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lstNewspieces As System.Windows.Forms.ListBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblStaticTags As System.Windows.Forms.Label
    Friend WithEvents cmdMoveUp As System.Windows.Forms.Button
    Friend WithEvents rtbTags As System.Windows.Forms.RichTextBox
    Friend WithEvents cmdMoveDown As System.Windows.Forms.Button
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblStaticDescription As System.Windows.Forms.Label
    Friend WithEvents lblSubtitle As System.Windows.Forms.Label
    Friend WithEvents lblStaticTitle As System.Windows.Forms.Label
    Friend WithEvents rtbDescription As System.Windows.Forms.RichTextBox
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents lblStaticSubtitle As System.Windows.Forms.Label
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblStaticDate As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblStaticTime As System.Windows.Forms.Label

End Class
