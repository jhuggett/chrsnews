﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TagCreation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstTags = New System.Windows.Forms.ListBox
        Me.lblCurrentTags = New System.Windows.Forms.Label
        Me.cmdAddNewTag = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.lblTagName = New System.Windows.Forms.Label
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.lblStaticTagName = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lstTags
        '
        Me.lstTags.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstTags.FormattingEnabled = True
        Me.lstTags.Location = New System.Drawing.Point(7, 22)
        Me.lstTags.Name = "lstTags"
        Me.lstTags.Size = New System.Drawing.Size(328, 342)
        Me.lstTags.TabIndex = 0
        '
        'lblCurrentTags
        '
        Me.lblCurrentTags.AutoSize = True
        Me.lblCurrentTags.Location = New System.Drawing.Point(7, 6)
        Me.lblCurrentTags.Name = "lblCurrentTags"
        Me.lblCurrentTags.Size = New System.Drawing.Size(88, 13)
        Me.lblCurrentTags.TabIndex = 1
        Me.lblCurrentTags.Text = "Registered Tags:"
        '
        'cmdAddNewTag
        '
        Me.cmdAddNewTag.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdAddNewTag.Location = New System.Drawing.Point(5, 323)
        Me.cmdAddNewTag.Name = "cmdAddNewTag"
        Me.cmdAddNewTag.Size = New System.Drawing.Size(96, 23)
        Me.cmdAddNewTag.TabIndex = 2
        Me.cmdAddNewTag.Text = "Create New Tag"
        Me.cmdAddNewTag.UseVisualStyleBackColor = True
        '
        'cmdRemove
        '
        Me.cmdRemove.Location = New System.Drawing.Point(57, 34)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(62, 23)
        Me.cmdRemove.TabIndex = 3
        Me.cmdRemove.Text = "Remove"
        Me.cmdRemove.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(12, 34)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(39, 23)
        Me.cmdEdit.TabIndex = 3
        Me.cmdEdit.Text = "Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'lblTagName
        '
        Me.lblTagName.AutoSize = True
        Me.lblTagName.Location = New System.Drawing.Point(44, 8)
        Me.lblTagName.Name = "lblTagName"
        Me.lblTagName.Size = New System.Drawing.Size(57, 13)
        Me.lblTagName.TabIndex = 4
        Me.lblTagName.Text = "Tag Name"
        '
        'cmdFinish
        '
        Me.cmdFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdFinish.Location = New System.Drawing.Point(109, 323)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(62, 23)
        Me.cmdFinish.TabIndex = 5
        Me.cmdFinish.Text = "Finish"
        Me.cmdFinish.UseVisualStyleBackColor = True
        '
        'lblStaticTagName
        '
        Me.lblStaticTagName.AutoSize = True
        Me.lblStaticTagName.Location = New System.Drawing.Point(9, 8)
        Me.lblStaticTagName.Name = "lblStaticTagName"
        Me.lblStaticTagName.Size = New System.Drawing.Size(29, 13)
        Me.lblStaticTagName.TabIndex = 6
        Me.lblStaticTagName.Text = "Tag:"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.cmdFinish)
        Me.Panel1.Controls.Add(Me.lblStaticTagName)
        Me.Panel1.Controls.Add(Me.cmdAddNewTag)
        Me.Panel1.Controls.Add(Me.cmdRemove)
        Me.Panel1.Controls.Add(Me.lblTagName)
        Me.Panel1.Controls.Add(Me.cmdEdit)
        Me.Panel1.Location = New System.Drawing.Point(341, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(181, 353)
        Me.Panel1.TabIndex = 7
        '
        'TagCreation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 380)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblCurrentTags)
        Me.Controls.Add(Me.lstTags)
        Me.MinimumSize = New System.Drawing.Size(321, 187)
        Me.Name = "TagCreation"
        Me.Text = "Tag Editor"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lstTags As System.Windows.Forms.ListBox
    Friend WithEvents lblCurrentTags As System.Windows.Forms.Label
    Friend WithEvents cmdAddNewTag As System.Windows.Forms.Button
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents lblTagName As System.Windows.Forms.Label
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents lblStaticTagName As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
