﻿Imports System
Imports System.IO
Imports System.Text

Public Class SetupWindowsForm

    Public PDM As PersistantDataManager

    Public frmMain As MainForm

    Private Sub cmdGenTagFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGenTagFile.Click
        Dim path As String = "tagFile.txt"
        Dim fs As FileStream = File.Create(path)

        fs.Close()

        subCheckFileStatus()


    End Sub


    Private Sub subCheckFileStatus()
        If PDM.fncTagsFileIsAvailable() Then
            lblTagFile.Text = "Found Tag File"
            cmdGenTagFile.Hide()
            cmdDeleteTagFile.Show()
        Else
            lblTagFile.Text = "Tag File Not Found"
            cmdGenTagFile.Show()
            cmdDeleteTagFile.Hide()
        End If

        If PDM.fncArchiveFileIsAvailable() Then
            lblAchiveFile.Text = "Found Archive File"
            cmdArchiveFile.Hide()
            cmdDeleteArchiveFile.Show()
        Else
            lblAchiveFile.Text = "Archive File Not Found"
            cmdArchiveFile.Show()
            cmdDeleteArchiveFile.Hide()
        End If
    End Sub


    Private Sub SetupWindowsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToParent()
        subCheckFileStatus()



    End Sub

    Private Sub cmdDeleteTagFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteTagFile.Click
        My.Computer.FileSystem.DeleteFile("tagFile.txt", Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.RecycleOption.DeletePermanently, Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)

        If PDM.fncTagsFileIsAvailable() Then
            PDM.subLoadTagsFromFile()
        Else
            PDM.subEmptyTags()
        End If

        subCheckFileStatus()

    End Sub

    Private Sub cmdArchiveFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdArchiveFile.Click
        Dim path As String = "archiveFile.txt"
        Dim fs As FileStream = File.Create(path)

        fs.Close()

        frmMain.subReloadData()

        subCheckFileStatus()
    End Sub

    Private Sub cmdDeleteArchiveFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeleteArchiveFile.Click
        My.Computer.FileSystem.DeleteFile("archiveFile.txt", Microsoft.VisualBasic.FileIO.UIOption.AllDialogs, Microsoft.VisualBasic.FileIO.RecycleOption.DeletePermanently, Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)

        PDM.doc = New NewsDocument()

        frmMain.subLoadPersistantData()
        frmMain.subReloadData()

        subCheckFileStatus()
    End Sub
End Class