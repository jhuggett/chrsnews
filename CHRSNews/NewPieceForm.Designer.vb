﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewPieceForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.clrDate = New System.Windows.Forms.MonthCalendar
        Me.rtbDescription = New System.Windows.Forms.RichTextBox
        Me.lblTitle = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.cbxIncludeTime = New System.Windows.Forms.CheckBox
        Me.cbxIncludeDate = New System.Windows.Forms.CheckBox
        Me.dtpTime = New System.Windows.Forms.DateTimePicker
        Me.txtTitle = New System.Windows.Forms.TextBox
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.lblError = New System.Windows.Forms.Label
        Me.lblSubtitle = New System.Windows.Forms.Label
        Me.txtSubtitle = New System.Windows.Forms.TextBox
        Me.lblAsterix = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblTags = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.clbTags = New System.Windows.Forms.CheckedListBox
        Me.cmdEditTags = New System.Windows.Forms.Button
        Me.pgbProgress = New System.Windows.Forms.ProgressBar
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdCancel.Location = New System.Drawing.Point(0, 64)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 0
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'clrDate
        '
        Me.clrDate.Location = New System.Drawing.Point(75, 137)
        Me.clrDate.MaxSelectionCount = 1
        Me.clrDate.MinDate = New Date(2018, 12, 12, 0, 0, 0, 0)
        Me.clrDate.Name = "clrDate"
        Me.clrDate.TabIndex = 24
        '
        'rtbDescription
        '
        Me.rtbDescription.Location = New System.Drawing.Point(75, 59)
        Me.rtbDescription.Name = "rtbDescription"
        Me.rtbDescription.Size = New System.Drawing.Size(227, 66)
        Me.rtbDescription.TabIndex = 23
        Me.rtbDescription.Text = ""
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Location = New System.Drawing.Point(38, 9)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(30, 13)
        Me.lblTitle.TabIndex = 22
        Me.lblTitle.Text = "Title:"
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(2, 59)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(66, 13)
        Me.lblDescription.TabIndex = 21
        Me.lblDescription.Text = "Description: "
        '
        'cbxIncludeTime
        '
        Me.cbxIncludeTime.AutoSize = True
        Me.cbxIncludeTime.Location = New System.Drawing.Point(75, 360)
        Me.cbxIncludeTime.Name = "cbxIncludeTime"
        Me.cbxIncludeTime.Size = New System.Drawing.Size(87, 17)
        Me.cbxIncludeTime.TabIndex = 20
        Me.cbxIncludeTime.Text = "Include Time"
        Me.cbxIncludeTime.UseVisualStyleBackColor = True
        '
        'cbxIncludeDate
        '
        Me.cbxIncludeDate.AutoSize = True
        Me.cbxIncludeDate.Location = New System.Drawing.Point(75, 311)
        Me.cbxIncludeDate.Name = "cbxIncludeDate"
        Me.cbxIncludeDate.Size = New System.Drawing.Size(87, 17)
        Me.cbxIncludeDate.TabIndex = 19
        Me.cbxIncludeDate.Text = "Include Date"
        Me.cbxIncludeDate.UseVisualStyleBackColor = True
        '
        'dtpTime
        '
        Me.dtpTime.CustomFormat = "h:mm tt"
        Me.dtpTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpTime.Location = New System.Drawing.Point(75, 334)
        Me.dtpTime.Name = "dtpTime"
        Me.dtpTime.ShowUpDown = True
        Me.dtpTime.Size = New System.Drawing.Size(72, 20)
        Me.dtpTime.TabIndex = 18
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(75, 6)
        Me.txtTitle.MaxLength = 48
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(227, 20)
        Me.txtTitle.TabIndex = 17
        '
        'cmdAdd
        '
        Me.cmdAdd.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.cmdAdd.Location = New System.Drawing.Point(83, 64)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(82, 23)
        Me.cmdAdd.TabIndex = 0
        Me.cmdAdd.Text = "Add"
        Me.cmdAdd.UseVisualStyleBackColor = True
        '
        'lblError
        '
        Me.lblError.ForeColor = System.Drawing.Color.Crimson
        Me.lblError.Location = New System.Drawing.Point(2, 9)
        Me.lblError.Name = "lblError"
        Me.lblError.Size = New System.Drawing.Size(161, 24)
        Me.lblError.TabIndex = 25
        Me.lblError.Text = "Error Message"
        Me.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblSubtitle
        '
        Me.lblSubtitle.AutoSize = True
        Me.lblSubtitle.Location = New System.Drawing.Point(23, 35)
        Me.lblSubtitle.Name = "lblSubtitle"
        Me.lblSubtitle.Size = New System.Drawing.Size(45, 13)
        Me.lblSubtitle.TabIndex = 21
        Me.lblSubtitle.Text = "Subtitle:"
        '
        'txtSubtitle
        '
        Me.txtSubtitle.Location = New System.Drawing.Point(75, 32)
        Me.txtSubtitle.MaxLength = 48
        Me.txtSubtitle.Name = "txtSubtitle"
        Me.txtSubtitle.Size = New System.Drawing.Size(227, 20)
        Me.txtSubtitle.TabIndex = 17
        '
        'lblAsterix
        '
        Me.lblAsterix.AutoSize = True
        Me.lblAsterix.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAsterix.ForeColor = System.Drawing.Color.Crimson
        Me.lblAsterix.Location = New System.Drawing.Point(308, 4)
        Me.lblAsterix.Name = "lblAsterix"
        Me.lblAsterix.Size = New System.Drawing.Size(15, 20)
        Me.lblAsterix.TabIndex = 26
        Me.lblAsterix.Text = "*"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Crimson
        Me.Label1.Location = New System.Drawing.Point(308, 59)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 20)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "*"
        '
        'lblTags
        '
        Me.lblTags.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTags.AutoSize = True
        Me.lblTags.Location = New System.Drawing.Point(341, 16)
        Me.lblTags.Name = "lblTags"
        Me.lblTags.Size = New System.Drawing.Size(126, 13)
        Me.lblTags.TabIndex = 28
        Me.lblTags.Text = "Tags(select at least one):"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Crimson
        Me.Label3.Location = New System.Drawing.Point(323, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(15, 20)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "*"
        '
        'clbTags
        '
        Me.clbTags.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clbTags.CheckOnClick = True
        Me.clbTags.FormattingEnabled = True
        Me.clbTags.Location = New System.Drawing.Point(344, 32)
        Me.clbTags.Name = "clbTags"
        Me.clbTags.Size = New System.Drawing.Size(219, 439)
        Me.clbTags.TabIndex = 29
        '
        'cmdEditTags
        '
        Me.cmdEditTags.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdEditTags.Location = New System.Drawing.Point(488, 7)
        Me.cmdEditTags.Name = "cmdEditTags"
        Me.cmdEditTags.Size = New System.Drawing.Size(75, 23)
        Me.cmdEditTags.TabIndex = 30
        Me.cmdEditTags.Text = "Edit Tags"
        Me.cmdEditTags.UseVisualStyleBackColor = True
        '
        'pgbProgress
        '
        Me.pgbProgress.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pgbProgress.Location = New System.Drawing.Point(0, 36)
        Me.pgbProgress.Name = "pgbProgress"
        Me.pgbProgress.Size = New System.Drawing.Size(165, 18)
        Me.pgbProgress.Step = 1
        Me.pgbProgress.TabIndex = 31
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.lblError)
        Me.Panel1.Controls.Add(Me.pgbProgress)
        Me.Panel1.Controls.Add(Me.cmdAdd)
        Me.Panel1.Controls.Add(Me.cmdCancel)
        Me.Panel1.Location = New System.Drawing.Point(75, 380)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(165, 96)
        Me.Panel1.TabIndex = 32
        '
        'NewPieceForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(566, 484)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdEditTags)
        Me.Controls.Add(Me.clbTags)
        Me.Controls.Add(Me.lblTags)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblAsterix)
        Me.Controls.Add(Me.clrDate)
        Me.Controls.Add(Me.rtbDescription)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.lblSubtitle)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.cbxIncludeTime)
        Me.Controls.Add(Me.cbxIncludeDate)
        Me.Controls.Add(Me.dtpTime)
        Me.Controls.Add(Me.txtSubtitle)
        Me.Controls.Add(Me.txtTitle)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(767, 636)
        Me.MinimumSize = New System.Drawing.Size(582, 523)
        Me.Name = "NewPieceForm"
        Me.Text = "NewPieceForm"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents clrDate As System.Windows.Forms.MonthCalendar
    Friend WithEvents rtbDescription As System.Windows.Forms.RichTextBox
    Friend WithEvents lblTitle As System.Windows.Forms.Label
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents cbxIncludeTime As System.Windows.Forms.CheckBox
    Friend WithEvents cbxIncludeDate As System.Windows.Forms.CheckBox
    Friend WithEvents dtpTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents lblError As System.Windows.Forms.Label
    Friend WithEvents lblSubtitle As System.Windows.Forms.Label
    Friend WithEvents txtSubtitle As System.Windows.Forms.TextBox
    Friend WithEvents lblAsterix As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTags As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clbTags As System.Windows.Forms.CheckedListBox
    Friend WithEvents cmdEditTags As System.Windows.Forms.Button
    Friend WithEvents pgbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
End Class
