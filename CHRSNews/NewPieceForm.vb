﻿

Public Class NewPieceForm

    Public frmPresentor As MainForm

    Public strRole As String = "create"
    Public intIndex As Integer = -1
    Public piece As NewsPiece

    Dim boolIncludeDate As Boolean = False
    Dim boolIncludeTime As Boolean = False

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.frmPresentor.Show()
        Me.Close()
    End Sub

    Private Sub cbxIncludeDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxIncludeDate.CheckedChanged
        boolIncludeDate = Not boolIncludeDate
        If cbxIncludeDate.Checked Then
            clrDate.Show()
        Else
            clrDate.Hide()
        End If
    End Sub

    Private Sub subLoadDataFromPiece()

        txtTitle.Text = piece.strTitle
        txtSubtitle.Text = piece.strSubtitle

        rtbDescription.Text = piece.strDescription

        If Not piece.fncReturnFormattedDate = "" Then
            cbxIncludeDate.Checked = True

            clrDate.SetDate(piece.dteDate)

        End If

        If Not piece.fncReturnFormattedTime = "" Then
            cbxIncludeTime.Checked = True

            dtpTime.Value = piece.dteTime

        End If

        For i = 0 To clbTags.Items.Count - 1
            If piece.tghTags.fncDoesContain(clbTags.Items(i).ToString()) Then
                clbTags.SetItemChecked(i, True)
            End If
        Next

        subUpdateProgressBar()

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.CenterToParent()

        clrDate.Hide()
        dtpTime.Hide()

        lblError.Hide()

        subDisplayTags()


        If strRole = "create" Then
            cmdAdd.Text = "Add"


        ElseIf strRole = "edit" Then
            cmdAdd.Text = "Save"

            subLoadDataFromPiece()
        End If



    End Sub

    Public Sub subDisplayTags()
        clbTags.Items.Clear()
        For Each item As String In frmPresentor.PDM.fncReturnTags().fncReturnTags()
            clbTags.Items.Add(item)
        Next
    End Sub

    Private Sub cbxIncludeTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxIncludeTime.CheckedChanged
        boolIncludeTime = Not boolIncludeTime
        If cbxIncludeTime.Checked Then
            dtpTime.Show()
        Else
            dtpTime.Hide()
        End If
    End Sub

    Private Sub subUpdateProgressBar()
        Dim intNumberOfRequirements As Integer = 3
        Dim intNumberOfRequirementsMet As Integer = 0
        If txtTitle.Text.Count > 0 Then
            intNumberOfRequirementsMet += 1
        End If

        If rtbDescription.Text.Count > 0 Then
            intNumberOfRequirementsMet += 1
        End If

        If clbTags.CheckedItems.Count > 0 Then
            intNumberOfRequirementsMet += 1
        End If

        pgbProgress.Value = 100 / intNumberOfRequirements * intNumberOfRequirementsMet
    End Sub

    Private Function fncCheckCompletion() As Boolean
        If txtTitle.Text.Count = 0 Then
            lblError.Text = "A Title Is Required"
            lblError.Show()

            Return False
        End If

        If rtbDescription.Text.Count = 0 Then
            lblError.Text = "A Description Is Required"
            lblError.Show()
            Return False
        End If

        If clbTags.CheckedItems.Count = 0 Then
            lblError.Text = "At least one tag is Required"
            lblError.Show()
            Return False
        End If

        lblError.Hide()
        Return True
    End Function

    Private Sub window_closing() Handles MyBase.Closing
        
    End Sub

    Private Function fncCreatePieceFromData() As NewsPiece
        Dim nspNew As New NewsPiece()
        ' strTitle strSubTitle dteDate dteTime strDescription 

        'nspNew.subInit(txtTitle.Text, txtSubtitle.Text, clrDate.MinDate)

        'nspNew.subInit(txtTitle.Text, rtbDescription.Text, txtSubtitle.Text, txtTags.Text)

        Dim tags As New ArrayList()

        For Each item As String In clbTags.CheckedItems
            tags.Add(item)
        Next

        Dim strTags As String = String.Join(",", TryCast(tags.ToArray(GetType(String)), String()))

        nspNew.subInit(txtTitle.Text, rtbDescription.Text, txtSubtitle.Text, strTags)

        If Me.boolIncludeDate Then

            nspNew.subSetDate(DateValue(clrDate.SelectionRange.Start.ToString()))
        End If

        If Me.boolIncludeTime Then
            nspNew.subSetTime(dtpTime.Value)
        End If

        Return nspNew
    End Function

    Private Sub subSavePiece()
        Dim nspNew As NewsPiece = fncCreatePieceFromData()

        Me.frmPresentor.subAddNewPiece(nspNew)
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click


        If fncCheckCompletion() Then
            If strRole = "create" Then
                subSavePiece()
                frmPresentor.lstNewspieces.SetSelected(frmPresentor.PDM.doc.nspNewsPieces.Count - 1, True)
            ElseIf strRole = "edit" Then
                frmPresentor.subSaveChangesToPieceAt(intIndex, fncCreatePieceFromData())
            End If




            Me.Close()
        End If
        
    End Sub

  
    Private Sub cmdEditTags_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditTags.Click
        Dim form As New TagCreation()

        form.frmMain = Me.frmPresentor

        form.frmNewsPiece = Me
        form.boolPresentedByNewPiece = True

        form.ShowDialog()
    End Sub

    Private Sub txtTitle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTitle.TextChanged
        subUpdateProgressBar()
    End Sub

    Private Sub rtbDescription_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtbDescription.TextChanged
        subUpdateProgressBar()
    End Sub

    Private Sub clbTags_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbTags.SelectedIndexChanged
        subUpdateProgressBar()
    End Sub

    Private Sub lblTitle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblTitle.Click

    End Sub

    Private Sub lblDescription_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblDescription.Click

    End Sub

    Private Sub lblError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblError.Click

    End Sub
End Class