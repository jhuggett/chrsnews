﻿Imports System
Imports System.Linq
Imports Newtonsoft.Json.Linq

Public Class PersistantDataManager

    Dim strTagFilePath As String = "tagFile.txt"
    Dim strArchiveFilePath As String = "archiveFile.txt"

    Dim tghAllTags As New TagHandler()

    Public doc As New NewsDocument()

    Public Function fncTagsFileIsAvailable() As Boolean

        If System.IO.File.Exists("tagFile.txt") Then
            Return True
        End If

        Return False

    End Function

    Public Function fncArchiveFileIsAvailable() As Boolean
        If System.IO.File.Exists("archiveFile.txt") Then
            Return True
        End If

        Return False

    End Function

    Public Sub subLoadTagsFromFile()

        Dim strFileContents As String
        strFileContents = My.Computer.FileSystem.ReadAllText(Me.strTagFilePath)

        Me.tghAllTags.subEmpty()

        For Each tag In strFileContents.Split(New Char() {","c})
            If Not tag = "" Then
                Me.tghAllTags.subAddTag(tag)
            End If

        Next

    End Sub

    Public Function fncReturnTags() As TagHandler

        Return Me.tghAllTags
    End Function

    Public Sub subEmptyTags()
        Me.tghAllTags.subEmpty()
    End Sub

    Public Sub subWriteToArchiveFile()
        System.IO.File.WriteAllText(strArchiveFilePath, Newtonsoft.Json.JsonConvert.SerializeObject(doc.nspNewsPieces, Newtonsoft.Json.Formatting.Indented))


    End Sub

    Public Sub subLoadArchiveFromFile()
        If Not Me.fncArchiveFileIsAvailable() Then
            Return
        End If

        Dim strContent As String = My.Computer.FileSystem.ReadAllText(strArchiveFilePath)

        doc = New NewsDocument()

        If strContent = "" Then

            Return
        End If

        'Dim dynamicObj = Newtonsoft.Json.JsonConvert.DeserializeObject(Of JObject)(res)
        'For Each prop As JProperty In dynamicObj.Properties()
        '    For Each subObj As JObject In prop.Value
        '        Console.WriteLine(subObj("show")("title"))
        '    Next
        'Next

        'Dim newDoc As NewsDocument = JsonConvert.DeserializeObject(Of NewsDocument)(strContent)

        'doc = newDoc

        Dim jObj = Newtonsoft.Json.JsonConvert.DeserializeObject(Of JArray)(strContent)
        Console.WriteLine("COUNT: " + jObj.Count.ToString())
        For Each subObj As JObject In jObj
            'Console.WriteLine(item.ToString())
            'Console.WriteLine("START")
            Dim newNewspiece As New NewsPiece()

            newNewspiece.strTitle = subObj.Value(Of String)("strTitle")

            newNewspiece.strSubtitle = subObj.Value(Of String)("strSubtile")

            newNewspiece.strDescription = subObj.Value(Of String)("strDescription")

            newNewspiece.dteDate = subObj.Value(Of Date)("dteDate").ToString()

            newNewspiece.dteTime = subObj.Value(Of Date)("dteTime").ToString()

            newNewspiece.tghTags = New TagHandler()

            For Each tag In subObj.Value(Of JObject)("tghTags").Properties().First.Values
                newNewspiece.tghTags.subAddTag(tag)
            Next

            doc.subAddPiece(newNewspiece)

            'Console.WriteLine(subObj.Value(Of String)("strTitle"))
            'Console.WriteLine(subObj.Value(Of String)("strSubtile"))
            'Console.WriteLine(subObj.Value(Of String)("strDescription"))
            'Console.WriteLine(subObj.Value(Of Date)("dteDate").ToString())
            'Console.WriteLine(subObj.Value(Of Date)("dteTime").ToString())
            'For Each tag In subObj.Value(Of JObject)("tghTags").Properties().First.Values
            'Console.WriteLine(tag)
            'Next
            'Console.WriteLine("END")

        Next


        'For i = 0 To doc.nspNewsPieces.ToArray().Length - 1

        '.nspNewsPieces.Add(Linq.JObject.Parse(doc.nspNewsPieces.ToArray()(i)))
        'Next





    End Sub


    Public Sub subWriteToTagsFile(ByVal tghTags As TagHandler)
        Dim strContent As String = ""

        For i = 0 To tghTags.fncReturnTags().Count - 1
            If Not i = tghTags.fncReturnTags().Count - 1 Then
                strContent += tghTags.fncReturnTags()(i) + ","
            Else
                strContent += tghTags.fncReturnTags()(i)
            End If
        Next

        System.IO.File.WriteAllText(strTagFilePath, strContent)

        tghAllTags = tghTags

    End Sub

End Class
