﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SetupWindowsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdGenTagFile = New System.Windows.Forms.Button
        Me.lblTagFile = New System.Windows.Forms.Label
        Me.lblAchiveFile = New System.Windows.Forms.Label
        Me.cmdArchiveFile = New System.Windows.Forms.Button
        Me.cmdDeleteTagFile = New System.Windows.Forms.Button
        Me.cmdDeleteArchiveFile = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cmdGenTagFile
        '
        Me.cmdGenTagFile.Location = New System.Drawing.Point(142, 4)
        Me.cmdGenTagFile.Name = "cmdGenTagFile"
        Me.cmdGenTagFile.Size = New System.Drawing.Size(59, 23)
        Me.cmdGenTagFile.TabIndex = 0
        Me.cmdGenTagFile.Text = "Create"
        Me.cmdGenTagFile.UseVisualStyleBackColor = True
        '
        'lblTagFile
        '
        Me.lblTagFile.AutoSize = True
        Me.lblTagFile.Location = New System.Drawing.Point(12, 9)
        Me.lblTagFile.Name = "lblTagFile"
        Me.lblTagFile.Size = New System.Drawing.Size(98, 13)
        Me.lblTagFile.TabIndex = 1
        Me.lblTagFile.Text = "Tag File Not Found"
        '
        'lblAchiveFile
        '
        Me.lblAchiveFile.AutoSize = True
        Me.lblAchiveFile.Location = New System.Drawing.Point(12, 45)
        Me.lblAchiveFile.Name = "lblAchiveFile"
        Me.lblAchiveFile.Size = New System.Drawing.Size(115, 13)
        Me.lblAchiveFile.TabIndex = 1
        Me.lblAchiveFile.Text = "Archive File Not Found"
        '
        'cmdArchiveFile
        '
        Me.cmdArchiveFile.Location = New System.Drawing.Point(142, 40)
        Me.cmdArchiveFile.Name = "cmdArchiveFile"
        Me.cmdArchiveFile.Size = New System.Drawing.Size(59, 23)
        Me.cmdArchiveFile.TabIndex = 0
        Me.cmdArchiveFile.Text = "Create"
        Me.cmdArchiveFile.UseVisualStyleBackColor = True
        '
        'cmdDeleteTagFile
        '
        Me.cmdDeleteTagFile.Location = New System.Drawing.Point(207, 4)
        Me.cmdDeleteTagFile.Name = "cmdDeleteTagFile"
        Me.cmdDeleteTagFile.Size = New System.Drawing.Size(59, 23)
        Me.cmdDeleteTagFile.TabIndex = 0
        Me.cmdDeleteTagFile.Text = "Delete"
        Me.cmdDeleteTagFile.UseVisualStyleBackColor = True
        '
        'cmdDeleteArchiveFile
        '
        Me.cmdDeleteArchiveFile.Location = New System.Drawing.Point(207, 40)
        Me.cmdDeleteArchiveFile.Name = "cmdDeleteArchiveFile"
        Me.cmdDeleteArchiveFile.Size = New System.Drawing.Size(59, 23)
        Me.cmdDeleteArchiveFile.TabIndex = 0
        Me.cmdDeleteArchiveFile.Text = "Delete"
        Me.cmdDeleteArchiveFile.UseVisualStyleBackColor = True
        '
        'SetupWindowsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(278, 79)
        Me.Controls.Add(Me.lblAchiveFile)
        Me.Controls.Add(Me.lblTagFile)
        Me.Controls.Add(Me.cmdDeleteArchiveFile)
        Me.Controls.Add(Me.cmdArchiveFile)
        Me.Controls.Add(Me.cmdDeleteTagFile)
        Me.Controls.Add(Me.cmdGenTagFile)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "SetupWindowsForm"
        Me.Text = "SetupWindowsForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdGenTagFile As System.Windows.Forms.Button
    Friend WithEvents lblTagFile As System.Windows.Forms.Label
    Friend WithEvents lblAchiveFile As System.Windows.Forms.Label
    Friend WithEvents cmdArchiveFile As System.Windows.Forms.Button
    Friend WithEvents cmdDeleteTagFile As System.Windows.Forms.Button
    Friend WithEvents cmdDeleteArchiveFile As System.Windows.Forms.Button
End Class
