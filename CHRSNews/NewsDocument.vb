﻿Imports System.Runtime.CompilerServices
Module RTFExtensions

    <Extension()> _
    Public Function ToRTF(ByVal s As String) As String
        Return "{\rtf1\ansi " + s + "}"
    End Function

    <Extension()> _
    Public Function ToBold(ByVal s As String) As String
        Return "\b {" + s + "} "
    End Function

    <Extension()> _
    Public Function NewLine(ByVal s As String) As String
        Return s + "\line "
    End Function

    <Extension()> _
    Public Function InPard(ByVal s As String) As String
        Return "\pard " + s + "\par"
    End Function

    <Extension()> _
    Public Function Underlined(ByVal s As String) As String
        Return "\ul1 " + s + "\ul0"
    End Function

    <Extension()> _
    Public Function Center(ByVal s As String) As String
        Return "\qc " + s
    End Function

    <Extension()> _
    Public Function Indent(ByVal s As String) As String
        Return "    " + s
    End Function

    <Extension()> _
    Public Function isEmpty(ByVal s As String) As String
        If s = "" Then
            Return True
        End If
        Return False
    End Function

End Module


Public Class NewsDocument

    Private strHeader As String
    Private strBody As String


    Public nspNewsPieces As New ArrayList()

    Public Sub subAddPiece(ByVal piece As NewsPiece)
        nspNewsPieces.Add(piece)
    End Sub

    Public Sub subCreateHeader(Optional ByVal namesToOffice As String = "")
        Me.strHeader = ""

        Dim currentDate As DateTime = DateTime.Now
        Dim daysOfWeek() As String = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}
        Me.strHeader += daysOfWeek(currentDate.DayOfWeek)
        Me.strHeader += ", "

        Dim monthsOfYear() As String = {"January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
        Me.strHeader += monthsOfYear(currentDate.Month - 1)
        Me.strHeader += " "

        Me.strHeader += CInt(currentDate.Day).ToString
        If currentDate.Day = 1 Then
            Me.strHeader += "st"
        ElseIf currentDate.Day = 2 Then
            Me.strHeader += "nd"
        ElseIf currentDate.Day = 3 Then
            Me.strHeader += "rd"
        Else
            Me.strHeader += "th"
        End If
        Me.strHeader += ", "
        Me.strHeader += CInt(currentDate.Year).ToString
        Me.strHeader = Me.strHeader.NewLine()


    End Sub

    Public Sub subCreateBody()
        Me.strBody = ""


        If Me.nspNewsPieces.Count = 0 Then
            Return
        End If



        For Each item As NewsPiece In Me.nspNewsPieces
            Dim strNewText As String = ""
            ' Add the title
            strNewText += (item.fncReturnTitle().Underlined() + ":").Indent()
            ' If has subtitle, add subtitle
            If Not item.fncReturnSubtitle = "" Then
                strNewText += " " + item.fncReturnSubtitle()
            End If

            ' Add date and time
            Dim strDate As String = item.fncReturnFormattedDate()
            If Not strDate = "" Then
                strNewText += "    " + strDate
            End If

            Dim strTime As String = item.fncReturnFormattedTime()
            If Not strTime = "" Then
                strNewText += " " + "at " + strTime
            End If

            strNewText = strNewText.InPard()

            strNewText += item.fncReturnDescription().InPard()

            strNewText = strNewText.NewLine().NewLine()

            strBody += strNewText
        Next

    End Sub

    Public Function fncReturnRTF() As String
        Dim strText As String = ""

        Me.subCreateHeader()

        strText += Me.strHeader.Center().InPard()

        Me.subCreateBody()

        strText += Me.strBody.InPard()

        strText = strText.ToRTF()

        Return strText
    End Function

    Public Sub subRemoveNewspieceAt(ByVal index As Integer)
        If index > -1 And index < nspNewsPieces.Count Then
            Me.nspNewsPieces.RemoveAt(index)
            Console.WriteLine("Removed at: " + index.ToString())
        End If

    End Sub

    Public Sub subMoveNewspieceUpAt(ByVal index As Integer)
        If index < 1 Then
            Return
        End If

        Dim newPieceReference As NewsPiece = nspNewsPieces.Item(index - 1)

        nspNewsPieces.RemoveAt(index - 1)

        nspNewsPieces.Insert(index, newPieceReference)


    End Sub

    Public Sub subMoveNewspieceDownAt(ByVal index As Integer)
        If index > nspNewsPieces.Count - 2 Then
            Return
        End If

        Dim newsPieceReference As NewsPiece = nspNewsPieces.Item(index)

        nspNewsPieces.RemoveAt(index)


        nspNewsPieces.Insert(index + 1, newsPieceReference)

    End Sub

End Class
